﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.Runtime.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace Oefeningenreeks6.TagHelpers
{
    [HtmlTargetElement("a")]
    public class CustomAnchorTagHelper : AnchorTagHelper
    {
        public CustomAnchorTagHelper(IHtmlGenerator generator) : base(generator)
        {
 
        }

        [HtmlAttributeName("asp-show-text")]
        public string DisplayText { set; get; }

        [HtmlAttributeName("asp-url")]
        public string Url { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.Attributes.Add(new TagHelperAttribute("href", Url));
            output.Content.SetContent(DisplayText);

        }

    }
}
